from django.contrib import admin
from .models import DatasetSources, OriginalDataset, TrainingDataset, AggregateResource, TestingDatasetSources, TestingOriginalDataset


admin.site.register(DatasetSources)
admin.site.register(OriginalDataset)
admin.site.register(TrainingDataset)
admin.site.register(AggregateResource)
admin.site.register(TestingDatasetSources)
admin.site.register(TestingOriginalDataset)