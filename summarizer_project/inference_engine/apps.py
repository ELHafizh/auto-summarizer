from django.apps import AppConfig


class InferenceEngineConfig(AppConfig):
    name = 'inference_engine'
