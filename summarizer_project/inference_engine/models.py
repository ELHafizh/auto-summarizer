from django.db import models
from django.contrib.postgres.fields import ArrayField
from django.utils import timezone


class DatasetSources(models.Model):
    title = models.CharField(default="None", max_length=255)
    path = models.FileField(upload_to='dataset/rawdatasets/')
    total_articles = models.IntegerField(default=0)
    is_originaldatasetpair = models.BooleanField(default=False)
    is_preprocessing = models.BooleanField(default=False)
    is_sentencescoring = models.BooleanField(default=False)
    is_scale_sentencescoring = models.BooleanField(default=False)
    is_bagging = models.BooleanField(default=False)
    is_randomforest = models.BooleanField(default=False)
    is_evaluate_oob = models.BooleanField(default=False)
    ground_truth = models.IntegerField(default=0)

    def __str__(self):
        return self.title

    def delete(self, *args, **kwargs):
        self.path.delete()
        super().delete(*args, **kwargs)


class OriginalDataset(models.Model):
    type_dataset = models.ForeignKey(DatasetSources, on_delete=models.CASCADE)
    category = models.CharField(max_length=255)
    title = models.CharField(max_length=255)
    json_line = models.TextField()
    source = models.CharField(max_length=100)
    extractive_summary = models.TextField()
    def __str__(self):
        return self.title


class TrainingDataset(models.Model):
    original_dataset = models.ForeignKey(OriginalDataset, on_delete=models.CASCADE)
    sentence_token = ArrayField(models.CharField(max_length=255))
    sentence = models.TextField()
    label = models.BooleanField()
    def __str__(self):
        return self.sentence


class PreprocessTrainingDataset(models.Model):
    original_dataset = models.ForeignKey(OriginalDataset, on_delete=models.CASCADE)
    sentence_token = ArrayField(models.CharField(max_length=255))
    sentence = models.TextField()
    label = models.BooleanField()
    def __str__(self):
        return self.sentence


class SentenceScoring(models.Model):
    original_dataset = models.ForeignKey(OriginalDataset, on_delete=models.CASCADE)
    sentence = models.TextField()
    f1_word_frequency = models.FloatField()
    f2_title_similarity = models.FloatField()
    f3_sentence_position = models.FloatField()
    f4_sentence_length = models.FloatField()
    f5_centrality = models.FloatField()
    f6_tf_isf = models.FloatField()
    f7_cosim = models.FloatField()
    f8_bigram = models.FloatField()
    f9_trigram = models.FloatField()
    label = models.BooleanField()
    def __str__(self):
        return self.sentence


class PreprocessSentenceScoring(models.Model):
    original_dataset = models.ForeignKey(OriginalDataset, on_delete=models.CASCADE)
    sentence = models.TextField()
    f1_word_frequency = models.FloatField()
    f2_title_similarity = models.FloatField()
    f3_sentence_position = models.FloatField()
    f4_sentence_length = models.FloatField()
    f5_centrality = models.FloatField()
    f6_tf_isf = models.FloatField()
    f7_cosim = models.FloatField()
    f8_bigram = models.FloatField()
    f9_trigram = models.FloatField()
    labels = models.FloatField()
    def __str__(self):
        return self.sentence


class AggregateResource(models.Model):
    type_dataset = models.OneToOneField(
        DatasetSources,
        on_delete=models.CASCADE,
        primary_key=True
    )
    fit_test_path = models.CharField(max_length=255)
    scaled_sentencescoring_path = models.CharField(max_length=255)
    bag_path = models.CharField(max_length=255)
    oob_path = models.CharField(max_length=255)
    rules_path = models.CharField(max_length=255)
    accuracy_oob = models.FloatField(default=0.0)
    accuracy_execution_time = models.CharField(default="", max_length=255)
    def __str__(self):
        return self.type_dataset.title


class LogOobAccuracy(models.Model):
    type_dataset = models.ForeignKey(DatasetSources, on_delete=models.CASCADE)
    dataset_name = models.CharField(max_length=255)
    accuracy = models.FloatField()
    execution_date = models.DateTimeField(default=timezone.now(), blank=True)
    execution_time = models.CharField(max_length=255)


class TestingDatasetSources(models.Model):
    title = models.CharField(default="None", max_length=255)
    path = models.FileField(upload_to='dataset/testdatasets/')
    total_articles = models.IntegerField(default=0)
    is_evaluate = models.BooleanField(default=False)
    saving_execution_time = models.CharField(default="", max_length=255)

    def __str__(self):
        return self.title

    def delete(self, *args, **kwargs):
        self.path.delete()
        super().delete(*args, **kwargs)


class TestingOriginalDataset(models.Model):
    type_dataset = models.ForeignKey(TestingDatasetSources, on_delete=models.CASCADE)
    category = models.CharField(max_length=255)
    title = models.CharField(max_length=255)
    json_line = models.TextField()
    source = models.CharField(max_length=100)
    reference_summary = models.TextField()
    system_summary = models.TextField()
    is_summarize = models.BooleanField(default=False)
    def __str__(self):
        return self.title


class SelectedRule(models.Model):
    current_rule = models.CharField(max_length=255)
    fit_test_path = models.CharField(max_length=255)
    is_selected = models.BooleanField(default=False)
    ground_truth = models.IntegerField(default=0)


class LogProcess(models.Model):
    dataset_name = models.CharField(max_length=255)
    process_name = models.CharField(max_length=255)
    execution_date = models.DateTimeField(default=timezone.now(), blank=True)
    execution_time = models.CharField(max_length=255)


class ReferenceSummaryToken(models.Model):
    original_dataset = models.ForeignKey(TestingOriginalDataset, on_delete=models.CASCADE)
    summary_token = ArrayField(models.CharField(max_length=255))


class SystemSummaryToken(models.Model):
    original_dataset = models.ForeignKey(TestingOriginalDataset, on_delete=models.CASCADE)
    summary_token = ArrayField(models.CharField(max_length=255))


class SummaryEvaluation(models.Model):
    original_dataset = models.OneToOneField(
        TestingOriginalDataset,
        on_delete=models.CASCADE,
        primary_key=True
    )
    recall = models.FloatField()
    precision = models.FloatField()
    f_measure = models.FloatField()
    execution_time = models.CharField(max_length=255)


class DatasetSummaryEvaluation(models.Model):
    type_dataset = models.OneToOneField(
        TestingDatasetSources,
        on_delete=models.CASCADE,
        primary_key=True
    )
    recall = models.FloatField()
    precision = models.FloatField()
    f_measure = models.FloatField()
    execution_time = models.CharField(max_length=255)

