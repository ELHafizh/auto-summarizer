from django.urls import path

from . import views

app_name = 'inference_engine'

urlpatterns = [
    # path('', views.index, name='index'),
    path('', views.index, name='index'),
    path('<int:dataset_id>', views.index, name='index'),
    path('dataset-list', views.dataset_list, name='dataset-list'),
    path('dataset-process', views.action_step_datasets, name='dataset-process'),
    path('dataset-process/<int:dataset_id>', views.action_step_datasets, name='dataset-process'),
    path('sentence-label', views.pairing_sentence_label, name='sentence-label'),
    path('display-training-dataset/<int:dataset_id>', views.display_training_dataset, name='display-training-dataset'),
    path('sentence-munging', views.sentence_munging, name='sentence-munging'),
    path('sentence-munging/<int:dataset_id>', views.display_sentence_munging, name='display-sentence-munging'),
    path('sentence-scoring', views.sentence_scoring, name='sentence-scoring'),
    path('sentence-scoring/<int:dataset_id>', views.display_sentence_scoring, name='display-sentence-scoring'),
    path('feature-scaling', views.feature_scaling, name='feature-scaling'),
    path('feature-scaling/<int:dataset_id>', views.display_feature_scaling, name='display-feature-scaling'),
    path('proceed-bagging', views.proceed_bagging, name='proceed-bagging'),
    path('random-forest', views.random_forest, name='random-forest'),
    path('upload-test-dataset', views.upload_test_dataset, name='upload-test-dataset'),
    path('summarize-article/<int:article_id>', views.summarize_article, name='summarize-article'),
    path('update-rule/<int:rule_id>', views.update_rule, name='update-rule'),
    path('evalute-oob', views.get_accuracy_from_oob, name='evaluate-oob'),
    path('evalute-article/<int:dataset_id>', views.test_generate_summary, name='evaluate-article'),
    path('evalute-dataset/<int:dataset_id>', views.test_generate_summaries, name='evaluate-dataset'),
    path('testing-statistics/<int:dataset_id>', views.proceed_statistics, name='testing-statistics'),
]