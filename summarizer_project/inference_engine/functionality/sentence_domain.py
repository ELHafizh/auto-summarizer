import os
import re
import json
from ..models import DatasetSources, AggregateResource, SelectedRule, LogProcess, TestingOriginalDataset, TestingDatasetSources, SummaryEvaluation, DatasetSummaryEvaluation
import spacy
import logging
from time import time
import datetime


logger = logging.getLogger(__name__)


def read_json_line(path):
    with open(path) as fileobj:
        for line in fileobj:
            yield line


def iterate_filedecorator(lines_injson):
    count_lines = 0
    try:
        while True:
            next(lines_injson)
            count_lines += 1
    except StopIteration:
        pass
    return count_lines


def concat_tokens_tosentence(words):
    final_join = ""
    for word1 in words:
        for word2 in word1:
            last_index = len(word2) - 1
            index = 0
            next_index = index + 1
            while index <= last_index:
                if next_index == last_index:
                    final_join = final_join + word2[index]
                    final_join = final_join + word2[next_index] + " "
                    index += 2
                    next_index = index + 1
                elif index <= next_index < len(word2):
                    if word2[next_index] == "," or word2[next_index] == ")":
                        final_join = final_join + word2[index]
                        final_join = final_join + word2[next_index] + " "
                        index += 2
                        next_index = index + 1
                    elif word2[next_index] == "-":
                        final_join = final_join + word2[index]
                        final_join = final_join + word2[next_index] + " "
                        index += 2
                        next_index = index + 1
                    else:
                        check_word = is_word(word2[index])
                        if check_word:
                            final_join = final_join + word2[index]
                            index += 1
                            next_index += 1
                        else:
                            final_join = final_join + word2[index] + " "
                            index += 1
                            next_index += 1
                else:
                    check_word = is_word(word2[index])
                    if check_word:
                        final_join = final_join + word2[index]
                        index += 1
                        next_index += 1
                    else:
                        final_join = final_join + word2[index] + " "
                        index += 1
                        next_index += 1
        final_join = final_join + "\n"
    return final_join


def is_word(input):
    w_regex = re.compile(r'\W|\.')
    match = w_regex.search(input)
    if match:
        return True
    return False


def is_number(input):
    num_regex = re.compile(r'\d')
    match = num_regex.search(input)
    if match:
        return True
    return False


def id_totitle_repr(input):
    patt_regex = re.compile(r'-\w*')
    match_with_hypen = patt_regex.findall(input)
    title = ""
    for list_ in match_with_hypen:
        word = re.findall(r'[^-]\w*', list_)
        word = "".join(word)
        title = title + word + " "
    return title


# Todo : create extractive summary representation from gold_label
def get_indexof_true_gold_labels(input):
    summary_index = []
    for i, j in enumerate(input):
        for k, l in enumerate(j):
            if l:
                current_bool = [i, k]
                summary_index.append(current_bool)
    return summary_index


def match_summary_index_into_paragraphs(paragraphs, gold_labels):
    summary_index = get_indexof_true_gold_labels(gold_labels)
    list_extract_summary = []
    for i in summary_index:
        list_extract_summary.append(paragraphs[i[0]][i[1]])
    extractive_summary = concat_tokens_tosummary(list_extract_summary)
    return extractive_summary


def match_summary_index_into_list(paragraphs, gold_labels):
    summary_index = get_indexof_true_gold_labels(gold_labels)
    list_extract_summary = []
    for i in summary_index:
        list_extract_summary.append(paragraphs[i[0]][i[1]])
    return list_extract_summary


def concat_tokens_tosummary(words):
    final_join = ""
    for word in words:
        last_index = len(word) - 1
        index = 0
        next_index = index + 1
        while index <= last_index:
            if next_index == last_index:
                final_join = final_join + word[index]
                final_join = final_join + word[next_index] + " "
                index += 2
                next_index = index + 1
            elif index <= next_index < len(word):
                if word[next_index] == "," or word[next_index] == ")":
                    final_join = final_join + word[index]
                    final_join = final_join + word[next_index] + " "
                    index += 2
                    next_index = index + 1
                elif word[next_index] == "-":
                    final_join = final_join + word[index]
                    final_join = final_join + word[next_index] + " "
                    index += 2
                    next_index = index + 1
                else:
                    check_word = is_word(word[index])
                    if check_word:
                        final_join = final_join + word[index]
                        index += 1
                        next_index += 1
                    else:
                        final_join = final_join + word[index] + " "
                        index += 1
                        next_index += 1
            else:
                check_word = is_word(word[index])
                if check_word:
                    final_join = final_join + word[index]
                    index += 1
                    next_index += 1
                else:
                    final_join = final_join + word[index] + " "
                    index += 1
                    next_index += 1
        final_join = final_join + "\n"
    return final_join


def saved_originaldataset(dataset_source, filepath, interval):
    lines_injson = read_json_line(filepath)
    for i in range(interval):
        lines = next(lines_injson)
        line = json.loads(lines)
        dataset_source.originaldataset_set.create(category=line['category'], title=id_totitle_repr(line['id']),
                                                  json_line=lines, source=line['source'],
                                                  extractive_summary=match_summary_index_into_paragraphs(
                                                      line['paragraphs'], line['gold_labels']))


def saved_test_originaldataset(dataset_source, filepath, interval):
    start = time()
    lines_injson = read_json_line(filepath)
    for i in range(interval):
        lines = next(lines_injson)
        line = json.loads(lines)
        testingoriginaldataset = dataset_source.testingoriginaldataset_set.create(
            category=line['category'], title=id_totitle_repr(line['id']), json_line=lines, source=line['source'], reference_summary=match_summary_index_into_paragraphs(
                line['paragraphs'], line['gold_labels']))
        structure_gold_labels = match_summary_index_into_list(line['paragraphs'], line['gold_labels'])
        for structure_gold_label in structure_gold_labels:
            testingoriginaldataset.referencesummarytoken_set.create(summary_token=structure_gold_label)
    end = time()
    result = end-start
    result = str(datetime.timedelta(seconds=result))
    TestingDatasetSources.objects.filter(pk=dataset_source.id).update(saving_execution_time=result)


def concat_listof_sentence(words):
    sentence_token = []
    for word1 in words:
        for word2 in word1:
            last_index = len(word2) - 1
            index = 0
            next_index = index + 1
            final_join = ""
            while index <= last_index:
                if next_index == last_index:
                    final_join = final_join + word2[index]
                    final_join = final_join + word2[next_index] + " "
                    index += 2
                    next_index = index + 1
                elif index <= next_index < len(word2):
                    if word2[next_index] == "," or word2[next_index] == ")":
                        final_join = final_join + word2[index]
                        final_join = final_join + word2[next_index] + " "
                        index += 2
                        next_index = index + 1
                    elif word2[next_index] == "-":
                        final_join = final_join + word2[index]
                        final_join = final_join + word2[next_index] + " "
                        index += 2
                        next_index = index + 1
                    else:
                        check_word = is_word(word2[index])
                        if check_word:
                            final_join = final_join + word2[index]
                            index += 1
                            next_index += 1
                        else:
                            final_join = final_join + word2[index] + " "
                            index += 1
                            next_index += 1
                else:
                    check_word = is_word(word2[index])
                    if check_word:
                        final_join = final_join + word2[index]
                        index += 1
                        next_index += 1
                    else:
                        final_join = final_join + word2[index] + " "
                        index += 1
                        next_index += 1
            sentence_token.append(final_join)
    return sentence_token


def echo_sentence_label_pairing(paragraphs, labels):
    result = []
    sentence_token = concat_listof_sentence(paragraphs)
    index_sentence_token = 0
    for sentence, label in zip(paragraphs, labels):
        limit = len(sentence) - 1
        index = 0
        pair = []
        while index <= limit:
            pair.append(sentence[index])
            pair.append(sentence_token[index_sentence_token])
            pair.append(label[index])
            index += 1
            index_sentence_token += 1
        result.append(pair)
    return result


"""
Sentence Munging
"""
from spacy.lang.id.stop_words import STOP_WORDS
from spacy.lang.id import LOOKUP

nlp = spacy.blank('id')


class SentenceMunging:

    def __init__(self, tokens):
        self._tokens = tokens
        self.remove_punctuations()
        self.case_folding()
        self.remove_stopword()
        self.lemmatization()

    @property
    def tokens(self):
        return self._tokens

    def remove_punctuations(self):
        temp_tokens = []
        regex_punct = re.compile(r'[:;\/\"\(\)\.\,!?\-\“\”]')
        regex_ws = re.compile(r'\s')
        regex_num = re.compile(r'\d')
        for index, token in enumerate(self._tokens):
            match_punct = regex_punct.search(token)
            match_ws = regex_ws.search(token)
            match_num = regex_num.search(token)
            if match_num:
                temp_tokens.append(self._tokens[index])
            elif match_punct or match_ws:
                pass
            else:
                temp_tokens.append(self._tokens[index])
        self._tokens = temp_tokens

    def case_folding(self):
        temp_tokens = []
        for token in self._tokens:
            temp_tokens.append(token.lower())
        self._tokens = temp_tokens

    def remove_stopword(self):
        temp_tokens = []
        for index, token in enumerate(self._tokens):
            doc = nlp(token)
            if doc[0].is_stop:
                pass
            else:
                temp_tokens.append(self._tokens[index])
        self._tokens = temp_tokens

    def lemmatization(self):
        temp_tokens = []
        for index, token in enumerate(self._tokens):
            doc = nlp(token)
            temp_tokens.append(doc[0].lemma_)
        self._tokens = temp_tokens


from collections import Counter
from math import log, sqrt
from nltk import ngrams


class SentenceScoring:

    def __init__(self, title, articles):
        self._articles = articles
        self._title = title
        self._score = {}
        self.word_frequency()
        self.title_similarity()
        self.sentence_position()
        self.sentence_length()
        self.centrality()
        self.tf_isf()
        self.calculate_cosim()
        self.bi_gram()
        self.tri_gram()

    @property
    def score(self):
        return self._score

    def word_frequency(self):
        list_of_word = []
        for article in self._articles:
            for word in article:
                list_of_word.append(word)
        self._word_counter = dict(Counter(list_of_word))
        list_frequency_sentence = []
        for article in self._articles:
            sentence_frequency_words = []
            for word in article:
                sentence_frequency_words.append(self._word_counter[word])
            result = 0
            for sentence_frequency_word in sentence_frequency_words:
                result += sentence_frequency_word
            list_frequency_sentence.append(result)
        self._score['f1_wf'] = list_frequency_sentence
        return list_frequency_sentence

    def title_similarity(self):
        title_tokens = []
        for title_token in self._title.split(" "):
            if title_token:
                title_tokens.append(title_token)
        score_lists = []
        title_len = len(title_tokens)
        for article in self._articles:
            score = 0
            for word in article:
                for token in title_tokens:
                    if word == token:
                        score += 1
            score_lists.append(score)
        for i, score_list in enumerate(score_lists):
            score_lists[i] = round((score_list / title_len), 2)
        self._score['f2_ts'] = score_lists
        return score_lists

    def sentence_position(self):
        n = len(self._articles)
        score_lists = []
        for i, article in enumerate(self._articles):
            if i == 0:
                score_lists.append(1)
            elif i == n - 1:
                score_lists.append(1)
            else:
                score_lists.append(round((((n - i) / n)), 2))
        self._score['f3_sp'] = score_lists
        return score_lists

    def sentence_length(self):
        index_max = 0
        for i, article in enumerate(self._articles):
            if len(article) > len(self._articles[index_max]):
                index_max = i
        score_lists = []
        for article in self._articles:
            score_lists.append(round((len(article) / len(self._articles[index_max])), 2))
        self._score['f4_sl'] = score_lists
        return score_lists

    def centrality(self):
        limit = len(self._articles)
        sentence_position = 0
        score_centrality = []
        for article in self._articles:
            pair_score = []
            for i in range(limit):
                if i != sentence_position:
                    vector1, vector2 = self.build_vector(article, self._articles[i])
                    score = round(self.cosine_similarity(vector1, vector2), 2)
                    pair_score.append(score)
            result = 0
            for score in pair_score:
                result += score
            score_centrality.append(round(result, 2))
            sentence_position += 1
        self._score['f5_centrality'] = score_centrality

    def tf_isf(self):
        list_isf = []
        list_tf = []
        list_len = []
        score_tf_isf = []
        for article in self._articles:
            score_isf = 0
            score_tf = 0
            article_counter = dict(Counter(article))
            list_len.append(len(article))
            for word in article:
                score_isf += self._word_counter[word]
            for k, v in article_counter.items():
                score_tf += v
            list_isf.append(score_isf)
            list_tf.append(score_tf)
        for tf, isf, length in zip(list_tf, list_isf, list_len):
            try:
                score = (log(isf) * tf) / length
                score_tf_isf.append(round(score, 2))
            except ValueError:
                score = 0
                score_tf_isf.append(score)
        self._score['f6_tfisf'] = score_tf_isf
        return score_tf_isf

    def build_vector(self, sentence, centroid):
        counter1 = Counter(sentence)
        counter2 = Counter(centroid)
        all_items = set(counter1.keys()).union(set(counter2.keys()))
        vector1 = [counter1[k] for k in all_items]
        vector2 = [counter2[k] for k in all_items]
        return vector1, vector2

    def cosine_similarity(self, vector1, vector2):
        dot_product = sum(n1 * n2 for n1, n2 in zip(vector1, vector2))
        magnitude1 = sqrt(sum(n ** 2 for n in vector1))
        magnitude2 = sqrt(sum(n ** 2 for n in vector2))
        try:
            result = dot_product / (magnitude1 * magnitude2)
        except ZeroDivisionError:
            result = 0
        return result

    def calculate_cosim(self):
        index_max = 0
        for i, value in enumerate(self._score['f6_tfisf']):
            if value > self._score['f6_tfisf'][index_max]:
                index_max = i
        score_cosim = []
        for article in self._articles:
            vector1, vector2 = self.build_vector(article, self._articles[index_max])
            score = round(self.cosine_similarity(vector1, vector2), 2)
            score_cosim.append(score)
        self._score['f7_cosim'] = score_cosim
        return score_cosim

    def bi_gram(self):
        score_bigram = []
        for article in self._articles:
            score = len(list(ngrams(article, 2)))
            score_bigram.append(score)
        self._score['f8_bigram'] = score_bigram
        return score_bigram

    def tri_gram(self):
        score_trigram = []
        for article in self._articles:
            score = len(list(ngrams(article, 3)))
            score_trigram.append(score)
        self._score['f9_trigram'] = score_trigram
        return score_trigram


from ..models import OriginalDataset
import pandas as pd


def dataset_into_dataframe(original_datasets):
    sentences, f1, f2, f3, f4, f5, f6, f7, f8, f9, labels = [], [], [], [], [], [], [], [], [], [], []
    for original_dataset in original_datasets:
        related_original_dataset = OriginalDataset.objects.get(pk=original_dataset.id)
        sentence_scorings = related_original_dataset.sentencescoring_set.all()
        for sentence_scoring in sentence_scorings[::-1]:
            sentences.append(sentence_scoring.sentence)
            f1.append(sentence_scoring.f1_word_frequency)
            f2.append(sentence_scoring.f2_title_similarity)
            f3.append(sentence_scoring.f3_sentence_position)
            f4.append(sentence_scoring.f4_sentence_length)
            f5.append(sentence_scoring.f5_centrality)
            f6.append(sentence_scoring.f6_tf_isf)
            f7.append(sentence_scoring.f7_cosim)
            f8.append(sentence_scoring.f8_bigram)
            f9.append(sentence_scoring.f9_trigram)
            labels.append(sentence_scoring.label)
    feature = ['f1', 'f2', 'f3', 'f4', 'f5', 'f6', 'f7', 'f8', 'f9', 'labels']
    data = list(zip(f1, f2, f3, f4, f5, f6, f7, f8, f9, labels))
    df = pd.DataFrame(data, columns=feature)
    return df, sentences


import numpy as np
from sklearn import preprocessing


class ComprehendDataset:
    """
    Input : Pandas Dataframe
    App Special Case :
        Before scaling all values, save the fit dataset condition into static csv file
    Specific instruction :
        1. begin_normalization()
        2. save dataframe into csv file as fit_test dataset
        3. final_normalization()
        4. scaling result dataframe save into csv file
    """

    def __init__(self, dataset, targetclass="end"):
        self._dataset = dataset
        self._df_beforescaled = self._dataset
        self._attributes = list(self._dataset)
        self._targetclass = targetclass
        if targetclass == "end":
            self._features = self._attributes
            self._feature_target = self._features[len(self._attributes) - 1]
            del self._features[len(self._attributes) - 1]
        elif targetclass == "begin":
            self._features = self._attributes
            self._feature_target = self._features[0]
            del self._features[0]
        self._check_missing_values = False

    @property
    def dataset(self):
        return self._dataset

    def begin_normalization(self):
        self.handling_missing_values()
        self.handling_outliers()
        self.central_tendency()

    def final_normalization(self):
        self.feature_scaling(self._targetclass)
        self.handling_missing_values()
        self.generate_five_number_summary()
        self.generate_target_class()

    def central_tendency(self):
        self._medians = pd.Series([self._dataset[feature].median() for feature in self._features])
        self._means = pd.Series([self._dataset[feature].mean() for feature in self._features])
        self._modes = pd.Series([self._dataset[feature].mode() for feature in self._features])
        self._list_of_Q1 = pd.Series([self._dataset[feature].quantile(0.25) for feature in self._features])
        self._list_of_Q3 = pd.Series([self._dataset[feature].quantile(0.75) for feature in self._features])
        self._list_of_IQR = pd.Series(self._list_of_Q3) - pd.Series(self._list_of_Q1)
        self._list_of_std = pd.Series([self._dataset[feature].std() for feature in self._features])
        self._list_of_std2 = self._list_of_std * 2
        self._list_of_std2_min = self._means - self._list_of_std2
        self._list_of_std2_max = self._means + self._list_of_std2
        self._upper_outliers = self._list_of_Q3 + (1.5 * self._list_of_IQR)
        self._lower_outliers = self._list_of_Q1 - (1.5 * self._list_of_IQR)
        self._list_of_min_val = pd.Series([self._dataset[feature].min() for feature in self._features])
        self._list_of_max_val = pd.Series([self._dataset[feature].max() for feature in self._features])

    def handling_outliers(self, distribution_type="nonstandard_distribution"):
        """
        option :
                1. if distribution_type = "nonstandard_distribution"
                    then
                        the value > outliers = Q1 + 1.5 * IQR
                        the value < outliers = Q1 - 1.5 * IQR
                2. if distribution_type = "standard_distribution"
                    then
                        the value > outliers = mean + std*2
                        the value < outliers = mean - std*2
        """
        self.central_tendency()
        if not self._check_missing_values:
            self.handling_missing_values()
        changes = []
        index = 0
        limit = len(self._features) - 1
        self._outliers = []
        if distribution_type == "standard_distribution":
            collection_of_outlier_threshold = list(zip(self._list_of_std2_min, self._list_of_std2_max))
        elif distribution_type == "nonstandard_distribution":
            collection_of_outlier_threshold = list(zip(self._lower_outliers, self._upper_outliers))
        else:
            return "set distribution_type parameter properly"
        while index <= limit:
            indexrow = 0
            for data in self._dataset[self._features[index]]:
                if data < collection_of_outlier_threshold[index][0]:
                    changes.append("On " + self._features[index] + " Min Before : " + str(data))
                    data = collection_of_outlier_threshold[index][0]
                    changes.append("After : " + str(data))
                    self._dataset[self._features[index]].iloc[indexrow] = collection_of_outlier_threshold[index][0]
                    self._outliers.append(self._dataset[self._features[index]].iloc[indexrow])
                elif data > collection_of_outlier_threshold[index][1]:
                    changes.append("On " + self._features[index] + " Max Before : " + str(data))
                    data = collection_of_outlier_threshold[index][1]
                    changes.append("After : " + str(data))
                    self._dataset[self._features[index]].iloc[indexrow] = collection_of_outlier_threshold[index][1]
                    self._outliers.append(self._dataset[self._features[index]].iloc[indexrow])
                indexrow += 1
            index += 1
        percentage = (len(self._outliers) / (len(self._dataset) * len(self._features))) * 100
        self._outliers_percentage = str(percentage) + "%"
        self.central_tendency()
        return changes

    def handling_missing_values(self, strategy="mean"):
        self._check_missing_values = True
        self.central_tendency()
        changes = []
        indexcolumn = 0
        for feature in self._features:
            indexrow = 0
            for data in self._dataset[feature]:
                if data == 0 or data == np.nan:
                    changes.append("On feature " + feature + " row " + str(indexrow) + " Before : " + str(data))
                    if strategy == "mean":
                        self._dataset[feature].iloc[indexrow] = self._means[indexcolumn]
                        changes.append("After filled by mean = " + str(self._dataset[feature].iloc[indexrow]))
                    elif strategy == "median":
                        self._dataset[feature].iloc[indexrow] = self._medians[indexcolumn]
                        changes.append("After filled by median = " + str(self._dataset[feature].iloc[indexrow]))
                    elif strategy == "mode":
                        self._dataset[feature].iloc[indexrow] = self._modes[indexcolumn]
                        changes.append("After filled by mode = " + str(self._dataset[feature].iloc[indexrow]))
                    else:
                        return "set strategy parameter properly"
                indexrow += 1
        self.central_tendency()
        if changes:
            return changes
        return "Missing values are none"

    def generate_five_number_summary(self):
        limit = len(self._features) - 1
        index = 0
        frame = pd.DataFrame(index="min q1 med q3 max".split())
        self.central_tendency()
        while index <= limit:
            frame[self._features[index]] = [self._list_of_min_val[index], self._list_of_Q1[index], self._medians[index],
                                            self._list_of_Q3[index], self._list_of_max_val[index]]
            index += 1
        self._five_number_summary = frame
        return frame

    def generate_target_class(self):
        self._classes = dict(Counter(self._dataset[self._feature_target]))
        return self._classes

    def feature_scaling(self, targetclass):
        scaler = preprocessing.MinMaxScaler()
        scaled_df = scaler.fit_transform(self._dataset)
        features = self._features
        features.append(self._feature_target)
        scaled_df = pd.DataFrame(scaled_df, columns=features)
        self._dataset = scaled_df
        self._attributes = list(self._dataset)
        if targetclass == "end":
            self._features = self._attributes
            self._feature_target = self._features[len(self._attributes) - 1]
            del self._features[len(self._attributes) - 1]
        self.central_tendency()


from os.path import exists
from os import makedirs

main_dir_loc = "/summarizer_project/static/dataset_reference/"
fit_test_dir = f"{main_dir_loc}fit_test/"
scaled_sentencescoring_dir = f"{main_dir_loc}scaled_sentencescoring/"
bag_dir = f"{main_dir_loc}bags/"
oob_dir = f"{main_dir_loc}oobs/"
rules_dir = f"{main_dir_loc}rules/"


def dataframe_into_csv(df, BASE_DIR, dir_name, dataset_id, dir_opt=0, index_file=0):
    """
    dir_opt =
        fit_test_dir : 0,
        scaled_sentencescoring_dir : 1,
        bag_dir : 2,
        oob_dir : 3,
        rules_dir : 4
    """
    switcher = {
        0: fit_test_dir,
        1: scaled_sentencescoring_dir,
        2: bag_dir,
        3: oob_dir,
        4: rules_dir
    }
    file_path_loc = f"{BASE_DIR}{switcher[dir_opt]}{dir_name}/"
    try:
        makedirs(file_path_loc)
    except FileExistsError:
        pass
    if dir_opt == 0 or dir_opt == 1 or dir_opt == 4:
        full_path = f"{file_path_loc}{dir_name}.csv"
    elif dir_opt == 2 or dir_opt == 3:
        full_path = f"{file_path_loc}{dir_name}_{index_file}.csv"
    df.to_csv(full_path, index=False)
    dataset_res = DatasetSources.objects.get(pk=dataset_id)
    if dir_opt == 0:
        agr_res = AggregateResource(type_dataset=dataset_res,
                                    fit_test_path=f"{switcher[dir_opt]}{dir_name}/{dir_name}.csv")
        agr_res.save()
    elif dir_opt == 1:
        agr_id = dataset_res.aggregateresource.type_dataset_id
        AggregateResource.objects.filter(pk=agr_id).update(
            scaled_sentencescoring_path=f"{switcher[dir_opt]}{dir_name}/{dir_name}.csv")
    elif dir_opt == 2:
        if index_file == 0:
            agr_id = dataset_res.aggregateresource.type_dataset_id
            AggregateResource.objects.filter(pk=agr_id).update(bag_path=f"{switcher[dir_opt]}{dir_name}")
    elif dir_opt == 3:
        if index_file == 0:
            agr_id = dataset_res.aggregateresource.type_dataset_id
            AggregateResource.objects.filter(pk=agr_id).update(oob_path=f"{switcher[dir_opt]}{dir_name}")


def dataframe_into_database(df, sentences, dataset_id, original_datasets):
    dataset_res = DatasetSources.objects.get(pk=dataset_id)
    index = 0
    for original_dataset in original_datasets:
        related_original_dataset = OriginalDataset.objects.get(pk=original_dataset.id)
        sentence_scorings = related_original_dataset.sentencescoring_set.all()
        for sentence_scoring in sentence_scorings[::-1]:
            preprocess_sentence_scoring = related_original_dataset.preprocesssentencescoring_set.create(
                sentence=sentence_scoring.sentence,
                f1_word_frequency=df['f1'][index],
                f2_title_similarity=df['f2'][index],
                f3_sentence_position=df['f3'][index],
                f4_sentence_length=df['f4'][index],
                f5_centrality=df['f5'][index],
                f6_tf_isf=df['f6'][index],
                f7_cosim=df['f7'][index],
                f8_bigram=df['f8'][index],
                f9_trigram=df['f9'][index],
                labels=df['labels'][index]
            )
            index += 1
    DatasetSources.objects.filter(pk=dataset_id).update(is_scale_sentencescoring=True)


from sklearn.utils import resample


class Bagging:

    def __init__(self, dataset, number_of_bootstrap=100):
        self._dataset = dataset
        self._attributes = list(self._dataset)
        self._features = self._attributes
        self._number_of_bootstrap = number_of_bootstrap
        self._feature_target = self._features[len(self._attributes) - 1]
        del self._features[len(self._attributes) - 1]
        self.sample_size()
        self.sample_attributes()
        self.bootstrap_aggregating()

    def sample_size(self):
        len_dataset = len(self._dataset.index)
        self._bootstrap_size = round((2 / 3 * len_dataset))

    def sample_attributes(self):
        self._listof_bagging_features = []
        bagging_feature = round(sqrt(len(self._features)))
        for i in range(self._number_of_bootstrap):
            self._listof_bagging_features.append(
                list(resample(self._features, replace=False, n_samples=bagging_feature)))

    def bootstrap_aggregating(self):

        self._listof_bagging = []
        self._listof_oob = []
        temp_bagging = []

        for i in range(self._number_of_bootstrap):

            start = time()

            features = self._listof_bagging_features[i]
            temp_features = features
            get_index = False
            bag_lists = []
            oob_lists = []

            for feature in features:
                if not get_index:
                    current_boot = resample(self._dataset[feature], replace=True, n_samples=self._bootstrap_size)
                    current_boot_index = list(current_boot.index)
                    real_data_index = [k for k in range(len(self._dataset.index))]
                    oob_index = [x for x in real_data_index if x not in current_boot_index]
                    bag_lists.append(list(current_boot))
                    get_index = True
                    oob_feature_target = []
                    current_boot_feature_target = []
                    for j in current_boot_index:
                        current_boot_feature_target.append(self._dataset[self._feature_target][j])
                else:
                    bag_list = []
                    for j in current_boot_index:
                        bag_list.append(self._dataset[feature][j])
                    bag_lists.append(bag_list)

            for k, feature in enumerate(self._features):
                oob_list = []
                for l in oob_index:
                    oob_list.append(self._dataset[feature][l])
                    if k == 0:
                        oob_feature_target.append(self._dataset[self._feature_target][l])
                oob_lists.append(oob_list)

            bag_lists.append(current_boot_feature_target)
            oob_lists.append(oob_feature_target)
            temp_features.append('labels')
            temp_features_attributes = self._attributes
            temp_features_attributes.append('labels')

            datum = {}
            for i in range(len(temp_features)):
                datum[temp_features[i]] = bag_lists[i]
            df = pd.DataFrame(data=datum)
            self._listof_bagging.append(df)
            datum = {}
            for i in range(len(temp_features_attributes)):
                datum[temp_features_attributes[i]] = oob_lists[i]
            df = pd.DataFrame(data=datum)
            self._listof_oob.append(df)

            end = time()
            result = end-start
            result = str(datetime.timedelta(seconds=result))
            print(f"Bagging -- Bag number-{i}: takes = {result}")

    @property
    def dataset(self):
        return self._dataset

    @property
    def bootstrap_size(self):
        return self._bootstrap_size

    @property
    def listof_bagging_features(self):
        return self._listof_bagging_features

    @property
    def listof_bagging(self):
        return self._listof_bagging

    @property
    def listof_oob(self):
        return self._listof_oob


class RandomForest:

    def __init__(self, list_of_bags, dataset_name=None):
        self._rules = []
        self._tree = {}
        self._list_of_bags = list_of_bags
        self._iter_left = True
        self._iter_right = False
        self._dataset_name = dataset_name
        self.random_forest_exec(self._list_of_bags)

    @property
    def rules(self):
        return self._rules

    def random_forest_exec(self, list_of_bags):
        for i, bag in enumerate(list_of_bags):
            start = time()

            self._dataset = bag
            self._attributes = list(self._dataset)
            self._features = self._attributes
            self._feature_target = self._features[len(self._attributes) - 1]
            self._dataset
            del self._features[len(self._attributes) - 1]
            gain_ratio_stats, index_main_interval = self.gain_ratio(self._dataset, self._features)
            dataset_partials, next_feature = self.get_max_gain(gain_ratio_stats, index_main_interval)
            for dataset in dataset_partials:
                compact_gain_ratio, compact_counter_index_interval = self.gain_ratio(dataset, next_feature)
                self.get_max_gain(compact_gain_ratio, compact_counter_index_interval, next_feature)
            self._rules.append(self._tree)
            self._tree = {}

            end = time()
            result = end-start
            result = str(datetime.timedelta(seconds=result))
            print(f"RANDOM FOREST -- Bag number-{i}: takes = {result}")

            if self._dataset_name:
                log_process = LogProcess(dataset_name=self._dataset_name, process_name=f"RANDOM FOREST -- Bag number-{i}", execution_time=result)
                log_process.save()

    def get_max_gain(self, gain_ratio_stats, index_main_interval, temp_feature=None):
        """Get interval with maximum gain ratio and append it into tree

        Parameters
        ----------
        gain_ratio_stats : list(pandas Dataframe)
        list of dataframe that contains interval, information gain, split information, and gain ratio
        index_main_interval : list(pandas Dataframe)
        list of dataframe that track interval counter and index with gain ratio value
        temp_feature : list(str)
        list of temporarily features, by default None

        Returns
        -------
        dataset_partials : list(pandas Dataframe)
        list of dataframe that contain the remaining dataset after maximum gain ratio feature selection
        next_feature : list(str)
        list that contains the remaining feature
        """
        list_compact_stat = []
        for gain_ratio_stat in gain_ratio_stats:
            compact_stat = {}
            list_gain_ratio = list(gain_ratio_stat['gain_ratio'])
            max_val = gain_ratio_stat['gain_ratio'].max()
            max_index = list_gain_ratio.index(max(list_gain_ratio))
            compact_stat[max_index] = max_val
            list_compact_stat.append(compact_stat)
        max_value = 0
        key_maximum = 0
        max_interval = 0
        for i, stat in enumerate(list_compact_stat):
            for key, value in stat.items():
                if max_value < value:
                    max_value = value
                    key_maximum = key
                    max_interval = gain_ratio_stats[i]['interval'][key_maximum]
        next_feature = []
        for i, stat in enumerate(list_compact_stat):
            for key, value in stat.items():
                if max_value == value:
                    index_max_feature = i
                else:
                    if temp_feature:
                        next_feature.append(temp_feature[i])
                    else:
                        next_feature.append(self._features[i])
        if 'root' not in self._tree:
            parent_max_feature = self._features[index_max_feature]
            self._tree['root'] = {}
            self._tree['root']['feature'] = parent_max_feature
            self._tree['root']['interval'] = max_value
            self._tree['root']['left_node'] = {}
            self._tree['root']['right_node'] = {}
        else:
            max_feature = temp_feature[index_max_feature]
            try:
                output_labels = index_main_interval[index_max_feature]['interval_partials'][key_maximum]
            except KeyError:
                for stat in list_compact_stat:
                    for key, value in stat.items():
                        if key_maximum > key:
                            key_maximum = key
                output_labels = index_main_interval[index_max_feature]['interval_partials'][key_maximum]
            final_decision = []
            list_key = []
            check_null_samples = False
            for output_label in output_labels:
                max_output = 0
                max_key = 0
                for key, values in output_label.items():
                    if max_output < values:
                        max_output = values
                        max_key = key
                    if values == 0:
                        check_null_samples = True
                    list_key.append(key)
                final_decision.append(max_key)
            list_key = set(list_key)
            list_key = list(list_key)
            check_decision = set(final_decision)
            if not check_null_samples:
                if len(output_labels[0]) == 2:
                    if len(check_decision) == 1:
                        final_decision = []
                        max_list_opt = []
                        for output_label in output_labels:
                            max_output = 0
                            max_key = 0
                            pact_opt = {}
                            for key, values in output_label.items():
                                if max_output < values:
                                    max_output = values
                                    max_key = key
                            pact_opt[max_key] = max_output
                            max_list_opt.append(pact_opt)
                        temp_opt = 0
                        temp_key = 0
                        loc_post = 0
                        for i, pact in enumerate(max_list_opt):
                            for key, values in pact.items():
                                if temp_opt < values:
                                    temp_opt = values
                                    temp_key = key
                                    loc_post = i
                        for i, output_label in enumerate(output_labels):
                            max_output = 0
                            max_key = 0
                            for key, values in output_label.items():
                                if max_output < values:
                                    max_output = values
                                    max_key = key
                            if i != loc_post:
                                if temp_key == max_key:
                                    if temp_key == list_key[0]:
                                        max_key = list_key[1]
                                    else:
                                        max_key = list_key[0]
                            final_decision.append(max_key)
            self.initiate_next_node(self._tree['root'], max_feature, max_interval, final_decision)
        next_datasets = index_main_interval[index_max_feature]['index_dataset'][key_maximum]
        dataset_partials = self.update_next_dataset(next_feature, next_datasets)
        self._dataset_part = dataset_partials
        next_iter = len(dataset_partials)
        del next_feature[len(next_feature) - 1]
        return dataset_partials, next_feature

    def initiate_next_node(self, rules_tree, max_feature, max_value, final_decision):
        for key, value in rules_tree.items():
            if key == 'left_node':
                if self._iter_left:
                    if 'interval' not in value:
                        value['feature'] = max_feature
                        value['interval'] = max_value
                        value['output'] = {}
                        value['output']['left'] = final_decision[0]
                        value['output']['right'] = final_decision[1]
                        self._iter_left = False
                        self._iter_right = True
                        return
            if key == 'right_node':
                if self._iter_right:
                    if 'interval' not in value:
                        value['feature'] = max_feature
                        value['interval'] = max_value
                        value['output'] = {}
                        value['output']['left'] = final_decision[0]
                        value['output']['right'] = final_decision[1]
                self._iter_left = True
                self._iter_right = False

    def generate_entropy(self, dataset, counters="none", general_entropy="none"):
        if counters == "none":
            class_counter = self.target_class_counter(dataset)
            total_sample = len(dataset.index)
            results = []
            for key, value in class_counter.items():
                calc = -(value / total_sample) * log((value / total_sample), 2)
                results.append(calc)
            entropy = 0
            for result in results:
                entropy += result
            return entropy, class_counter
        else:
            total_value = 0
            total_samples = []
            for counter in counters:
                total_sample = 0
                for key, value in counter.items():
                    total_value += value
                    total_sample += value
                total_samples.append(total_sample)
            compact_entropy = []
            information_gain = general_entropy
            split_information = 0
            result = 0
            for i, counter in enumerate(counters):
                entropy = 0
                for key, value in counter.items():
                    try:
                        entropy += -(value / total_samples[i]) * log((value / total_samples[i]), 2)
                    except (ZeroDivisionError, ValueError):
                        entropy += 0
                try:
                    split_information += -(total_samples[i] / total_value) * log((total_samples[i] / total_value), 2)
                except (ZeroDivisionError, ValueError):
                    split_information += 0
                result += (total_samples[i] / total_value) * entropy
            information_gain -= result
            try:
                gain_ratio = information_gain / split_information
            except (ZeroDivisionError, ValueError):
                gain_ratio = 0
            return information_gain, split_information, gain_ratio

    def target_class_counter(self, dataset):
        return dict(Counter(dataset[self._feature_target]))

    def gain_ratio(self, dataset, features):
        """Calculate Gain Ratio

        Parameters
        ----------
        dataset : pandas Dataframe
        feature : list (str)

        Returns
        -------
        compact_gain_ratio : list(pandas Dataframe)
        list of dataframe that contains interval, information gain, split information, and gain ratio
        compact_counter_index_interval : list(pandas Dataframe)
        list of dataframe that track interval counter and index with gain ratio value
        """
        compact_gain_ratio = []
        compact_counter_index_interval = []
        general_entropy, class_counter = self.generate_entropy(dataset)
        for feature in features:
            intervals = list(set(list(dataset[feature])))
            intervals.sort()
            interval_partials = []
            dataset_index_partials = []
            ig_list = []
            split_information_list = []
            gain_ratio_list = []
            for interval in intervals:
                interval_partial = []
                dataset_index_partial = []
                counter_interval_lower = {}
                counter_interval_upper = {}
                dataset_index_lower = {}
                dataset_index_upper = {}
                for i, value in enumerate(list(dataset[feature])):
                    if value <= interval:
                        if dataset[self._feature_target][i] in counter_interval_lower:
                            counter_interval_lower[dataset[self._feature_target][i]] += 1
                            dataset_index_lower[dataset[self._feature_target][i]].append(i)
                        else:
                            counter_interval_lower[dataset[self._feature_target][i]] = 1
                            dataset_index_lower[dataset[self._feature_target][i]] = []
                            dataset_index_lower[dataset[self._feature_target][i]].append(i)
                    else:
                        if dataset[self._feature_target][i] in counter_interval_upper:
                            counter_interval_upper[dataset[self._feature_target][i]] += 1
                            dataset_index_upper[dataset[self._feature_target][i]].append(i)
                        else:
                            counter_interval_upper[dataset[self._feature_target][i]] = 1
                            dataset_index_upper[dataset[self._feature_target][i]] = []
                            dataset_index_upper[dataset[self._feature_target][i]].append(i)
                if not counter_interval_lower:
                    for key, value in class_counter.items():
                        counter_interval_lower[key] = 0
                if not counter_interval_upper:
                    for key, value in class_counter.items():
                        counter_interval_upper[key] = 0
                if len(counter_interval_lower) != len(class_counter):
                    for key, value in class_counter.items():
                        if key not in counter_interval_lower:
                            counter_interval_lower[key] = 0
                if len(counter_interval_upper) != len(class_counter):
                    for key, value in class_counter.items():
                        if key not in counter_interval_upper:
                            counter_interval_upper[key] = 0
                if not dataset_index_lower:
                    for key, value in class_counter.items():
                        dataset_index_lower[key] = []
                if not dataset_index_upper:
                    for key, value in class_counter.items():
                        dataset_index_upper[key] = []
                if len(dataset_index_lower) != len(class_counter):
                    for key, value in class_counter.items():
                        if key not in dataset_index_lower:
                            dataset_index_lower[key] = []
                if len(dataset_index_upper) != len(class_counter):
                    for key, value in class_counter.items():
                        if key not in dataset_index_upper:
                            dataset_index_upper[key] = []
                compact = []
                interval_partial.append(counter_interval_lower)
                interval_partial.append(counter_interval_upper)
                dataset_index_partial.append(dataset_index_lower)
                dataset_index_partial.append(dataset_index_upper)
                dataset_index_partials.append(dataset_index_partial)
                ig, split_information, gain_ratio = self.generate_entropy(dataset, interval_partial, general_entropy)
                interval_partials.append(interval_partial)
                ig_list.append(ig)
                split_information_list.append(split_information)
                gain_ratio_list.append(gain_ratio)
            datum = {'interval': intervals, 'ig': ig_list, 'split_information': split_information_list,
                     'gain_ratio': gain_ratio_list}
            df = pd.DataFrame(data=datum)
            compact_gain_ratio.append(df)
            datum = {'interval': intervals, 'interval_partials': interval_partials,
                     'index_dataset': dataset_index_partials}
            df = pd.DataFrame(data=datum)
            compact_counter_index_interval.append(df)
        return compact_gain_ratio, compact_counter_index_interval

    def update_next_dataset(self, next_feature, next_datasets):
        bound = len(next_datasets)
        list_of_dataset = []
        next_feature.append(self._feature_target)
        for i in range(bound):
            datum = {}
            for feature in next_feature:
                for key, indexes in next_datasets[i].items():
                    for index in indexes:
                        if feature in datum:
                            datum[feature].append(self._dataset[feature][index])
                        else:
                            datum[feature] = []
                            datum[feature].append(self._dataset[feature][index])
            df = pd.DataFrame(data=datum)
            list_of_dataset.append(df)
        return list_of_dataset


def save_rules(rules, BASE_DIR, rules_dir, dataset_id):
    file_path_loc = f"{BASE_DIR}{rules_dir}"
    try:
        makedirs(file_path_loc)
    except FileExistsError:
        pass
    for rule in rules:
        with open(f"{file_path_loc}/decisions.txt", 'a') as file:
            file.writelines(json.dumps(rule))
            file.writelines("\n")
        with open(f"{file_path_loc}/decisions_viz.txt", 'a') as file:
            file.writelines(json.dumps(rule, sort_keys=True, indent=4))
            file.writelines("\n")
    dataset_res = DatasetSources.objects.get(pk=dataset_id)
    agr_id = dataset_res.aggregateresource.type_dataset_id
    AggregateResource.objects.filter(pk=agr_id).update(rules_path=f"{rules_dir}/decisions.txt")
    agr_res = AggregateResource.objects.get(pk=agr_id)
    selectedrules = SelectedRule.objects.all()
    if selectedrules:
        selectedrule = SelectedRule.objects.get(pk=selectedrules[0].id)
        selectedrule.current_rule = agr_res.rules_path
        selectedrule.fit_test_path = agr_res.fit_test_path
        selectedrule.ground_truth = dataset_res.ground_truth
        selectedrule.save()
    else:
        selectedrule = SelectedRule(current_rule=agr_res.rules_path, fit_test_path=agr_res.fit_test_path,
                                    is_selected=True, ground_truth=dataset_res.ground_truth)
        selectedrule.save()
    DatasetSources.objects.filter(pk=dataset_id).update(is_randomforest=True)


def evaluate_oob(oob_path, rule_path):
    """Look for accuracy from Out-Of-Bag dataset

    Parameters
    ----------
    oob_path : str
    oob dataset file location
    rule_path : str
    rules file location

    Returns
    ------- 
    accuracy_score : float
    Accuracy of confusion matrix
    """

    # get real number labels
    df_oob = pd.read_csv(oob_path)
    real_labels = list(df_oob['labels'])

    # get active rule
    rules = read_json_line(rule_path)
    count_lines = iterate_filedecorator(rules)

    # get the overall summary of each rule
    votes = {}
    for i in range(len(df_oob)):
        vote = []
        rules = read_json_line(rule_path)
        for j in range(count_lines):
            current_rule = next(rules)
            current_rule = json.loads(current_rule)
            if df_oob.loc[i][current_rule['root']['feature']] <=current_rule['root']['interval']:
                if df_oob.loc[i][current_rule['root']['left_node']['feature']] <= current_rule['root']['left_node']['interval']:
                    vote.append(current_rule['root']['left_node']['output']['left'])
                else:
                    vote.append(current_rule['root']['left_node']['output']['right'])
            else:
                if df_oob.loc[i][current_rule['root']['right_node']['feature']] <= current_rule['root']['right_node']['interval']:
                    vote.append(current_rule['root']['right_node']['output']['left'])
                else:
                    vote.append(current_rule['root']['right_node']['output']['right'])
        votes[i] = vote

    # count the total labels for each rule
    vote_compose = {}
    for key, value in votes.items():
        vote_compose[key] = dict(Counter(value))

    # choose the label with the majority vote
    vote_decisions = []
    for key, value in vote_compose.items():
        key_decision = max(value, key=value.get)
        vote_decisions.append(key_decision)

    # Get accuracy
    # Find TP = Predicted Yes, Actual Yes
    # Find TN  = Predicted No, Actual No
    true_positive = 0
    true_negative = 0
    for real, system in zip(real_labels, vote_decisions):
        if real:
            if system:
                true_positive += 1
        else:
            if not system:
                true_negative += 1

    # accuracy = (tp+tn) / total
    accuracy_score = (true_positive+true_negative) / len(real_labels)

    return accuracy_score


def extract_ground_truth(id_dataset):
    gt_dsource = DatasetSources.objects.get(pk=id_dataset)
    gt_ordtset = gt_dsource.originaldataset_set.all()
    list_true_counter = []
    for i in gt_ordtset:
        gt_raw_jsonline = json.loads(i.json_line)
        true_counter = 0
        for j in gt_raw_jsonline['gold_labels']:
            for k in j:
                if k:
                    true_counter += 1
        list_true_counter.append(true_counter)
    total_count = 0
    for i in list_true_counter:
        total_count += i
    ground_truth = total_count / len(gt_ordtset)
    return round(ground_truth)


"""
SUMMARY EVALUATION
"""


def sentence_score_from_testing_data(original_dataset):
    """Looking for the score of each article sentence in the testing dataset

    Parameters
    ----------
    original_dataset : obj models
    Object Models of TestingOriginalDataset in the Database

    Returns
    -------
    df : pandas Dataframe
    Dataframe that represent sentence scoring
    """
    # pairing sentence with label
    article = json.loads(original_dataset.json_line)
    results = echo_sentence_label_pairing(article['paragraphs'], article['gold_labels'])
    articles = []
    labels = []

    # Preprocess Sentences
    for result in results:
        if len(result) > 3:
            ordinal = len(result) // 3
            track_counter = 0
            for counter in range(ordinal):
                sentence_munging = SentenceMunging(result[track_counter])
                track_counter  += 2
                articles.append(sentence_munging.tokens)
                labels.append(result[track_counter])
                track_counter  += 1
        else:
            sentence_munging = SentenceMunging(result[0])
            articles.append(sentence_munging.tokens)
            labels.append(result[2])

    # Sentence Scoring
    sentence_scoring = SentenceScoring(original_dataset.title, articles)
    sentence_scoring.score['labels'] = labels
    feature = ['f1', 'f2', 'f3', 'f4', 'f5', 'f6', 'f7', 'f8', 'f9', 'labels']
    score_pair = sentence_scoring.score
    data = list(zip(score_pair['f1_wf'], score_pair['f2_ts'], score_pair['f3_sp'], score_pair['f4_sl'], \
                score_pair['f5_centrality'], score_pair['f6_tfisf'], score_pair['f7_cosim'], \
                score_pair['f8_bigram'], score_pair['f9_trigram'], score_pair['labels']))
    df = pd.DataFrame(data, columns=feature)
    return df


def scale_and_extract_rule_path(df, BASE_DIR):
    """Looking for the score of each article sentence in the testing dataset

    Parameters
    ----------
    df : pandas Dataframe
    Dataframe that represent sentence scoring

    Returns
    -------
    df : pandas Dataframe
    Scaled Dataframe
    rule_path : str
    active rule location
    count_lines : int
    total rules
    """
    # get fit_test_path
    selected_rule = SelectedRule.objects.get(pk=SelectedRule.objects.all()[0].id)
    rule_path = selected_rule.current_rule
    fit_test_path = f"{BASE_DIR}{selected_rule.fit_test_path}"
    df_fit_test_path = pd.read_csv(fit_test_path)
    feature = ['f1', 'f2', 'f3', 'f4', 'f5', 'f6', 'f7', 'f8', 'f9', 'labels']

    # handling missing values
    from sklearn.impute import SimpleImputer
    imp = SimpleImputer(missing_values=np.nan, strategy='mean')
    imp.fit(df_fit_test_path)
    df = imp.transform(df)
    df = pd.DataFrame(df, columns=feature)

    # scaling
    from sklearn import preprocessing
    scaler = preprocessing.MinMaxScaler()
    scaler.fit(df_fit_test_path)
    df = scaler.transform(df)
    df = pd.DataFrame(df, columns=feature)
    df_labels = df['labels']
    df = df.drop(['labels'], axis=1)

    # read rules_path
    rules = read_json_line(f"{BASE_DIR}{rule_path}")
    count_lines = iterate_filedecorator(rules)
    rules = read_json_line(f"{BASE_DIR}{rule_path}")
    return df, rule_path, count_lines


def ensemble_votes(df, rules_path, count_lines, article):
    """Looking for the score of each article sentence in the testing dataset

    Parameters
    ----------
    df : pandas Dataframe
    Scaled Dataframe
    rule_path : str
    active rule location
    count_lines : int
    total rules

    Returns
    -------
    vote_compose : dict
    summary of vote decisions
    structure_gold_labels : list
    pseudo gold_labels of json_line from system summary 
    """
    votes = {}
    for i in range(len(df)):
        vote = []
        rules = read_json_line(rules_path)
        for j in range(count_lines):
            current_rule = next(rules)
            current_rule = json.loads(current_rule)
            if df.loc[i][current_rule['root']['feature']] <= current_rule['root']['interval']:
                if df.loc[i][current_rule['root']['left_node']['feature']] <= current_rule['root']['left_node']['interval']:
                    vote.append(current_rule['root']['left_node']['output']['left'])
                else:
                    vote.append(current_rule['root']['left_node']['output']['right'])
            else:
                if df.loc[i][current_rule['root']['right_node']['feature']] <= current_rule['root']['right_node']['interval']:
                    vote.append(current_rule['root']['right_node']['output']['left'])
                else:
                    vote.append(current_rule['root']['right_node']['output']['right'])
        votes[i] = vote
    vote_compose = {}
    for key, value in votes.items():
        vote_compose[key] = dict(Counter(value))
    
    list_vc = []
    for i in vote_compose:
        list_vc.append(vote_compose[i][1.0])
    sorted_list_vc = sorted(list_vc)
    sorted_list_vc = sorted_list_vc[::-1]
    selected_rule = SelectedRule.objects.get(pk=SelectedRule.objects.all()[0].id)
    limit_gt = selected_rule.ground_truth
    index_gt = []
    counter_gt = 0

    while counter_gt != limit_gt:
        for j, k in enumerate(list_vc):
            if len(sorted_list_vc)-1 != counter_gt-1:
                if sorted_list_vc[counter_gt] == k:
                    if counter_gt != limit_gt:
                        index_gt.append(j)
                        counter_gt += 1

    index_gt = sorted(index_gt)

    # article['gold_labels'] for system summary
    structure_gold_labels = []
    for i in article['gold_labels']:
        value = []
        for j in i:
            value.append(False)
        structure_gold_labels.append(value)

    limit_index_gt = len(index_gt) - 1
    track_index_gt = 0
    counter = 0
    for i,j in enumerate(structure_gold_labels):
        for k, l in enumerate(j):
            if index_gt[track_index_gt] == counter:
                if track_index_gt <= limit_index_gt:
                    structure_gold_labels[i][k] = True
                    if track_index_gt != limit_index_gt:
                        track_index_gt += 1
            counter += 1
    
    return vote_compose, structure_gold_labels


def rouge_evaluation(original_dataset, structure_gold_labels):
    """Looking for f-measure values with ROUGE-2 evaluation

    Parameters
    ----------
    original_dataset : obj Models\n
    Representative of the TestingOriginalDataset object in the database\n
    structure_gold_labels : list\n
    pseudo gold_labels of json_line from system summary\n

    Returns
    -------
    f_measure : float\n
    precision : float\n
    recall : float
    """
    article = json.loads(original_dataset.json_line)

    reference_summary_list = match_summary_index_into_list(article['paragraphs'], article['gold_labels'])
    system_summary_list = match_summary_index_into_list(article['paragraphs'], structure_gold_labels)

    # Sentence Munging
    for i, sentence in enumerate(reference_summary_list):
        sentence_munging = SentenceMunging(sentence)
        reference_summary_list[i] = sentence_munging.tokens
    for i, sentence in enumerate(system_summary_list):
        sentence_munging = SentenceMunging(sentence)
        system_summary_list[i] = sentence_munging.tokens

    overlapping_bigram = 0

    # total bi-gram
    total_bigram_reference_summary = 0
    total_bigram_system_summary = 0

    for i in reference_summary_list:
        total_bigram_reference_summary += len(list(ngrams(i, 2)))
    for i in system_summary_list:
        total_bigram_system_summary += len(list(ngrams(i, 2)))

    # find number of overlapping bi-gram
    for reference_summary in reference_summary_list:
        reference_summary = list(ngrams(reference_summary, 2))
        for system_summary in system_summary_list:
            system_summary = list(ngrams(system_summary,2))
            for reference_bigrams in reference_summary:
                for system_bigrams in system_summary:
                    if len(reference_bigrams) == len(system_bigrams):
                        check_true = []
                        for reference_bigram in reference_bigrams:
                            for system_bigram in system_bigrams:
                                if reference_bigram == system_bigram:
                                    check_true.append(True)
                        if len(check_true) == len(reference_bigrams):
                            overlapping_bigram += 1

    recall = overlapping_bigram / total_bigram_reference_summary
    precision = overlapping_bigram / total_bigram_system_summary

    try:
        f_measure = (2*precision*recall) / (precision + recall)
    except (ZeroDivisionError):
        f_measure = 0

    # print(f"recall = {recall}\nprecision = {precision}\nf_measure = {f_measure}")

    return f_measure, precision, recall


def article_rouge_evaluation(original_dataset, BASE_DIR):
    start = time()
    article = json.loads(original_dataset.json_line)
    df = sentence_score_from_testing_data(original_dataset)
    df, rule_path, count_lines = scale_and_extract_rule_path(df, BASE_DIR)
    vote_compose, structure_gold_labels = ensemble_votes(
        df, f"{BASE_DIR}{rule_path}", count_lines, article)
    TestingOriginalDataset.objects.filter(pk=original_dataset.id).update(
        system_summary=match_summary_index_into_paragraphs(article['paragraphs'], structure_gold_labels), is_summarize=True)
    for structure_gold_label in structure_gold_labels:
        original_dataset.systemsummarytoken_set.create(
            summary_token=structure_gold_label)
    f_measure, precision, recall = rouge_evaluation(
        original_dataset, structure_gold_labels)
    end = time()
    result = end-start
    result = str(datetime.timedelta(seconds=result))
    SummaryEvaluation.objects.create(original_dataset=original_dataset, recall=recall,
                                     precision=precision, f_measure=f_measure, execution_time=result)
    return f_measure, precision, recall


def proceed_rouge_evaluation(testingdatasetsource, BASE_DIR):
    """TestingDatasetSources Evaluation"""
    start = time()
    original_datasets = testingdatasetsource.testingoriginaldataset_set.all()

    recalls = 0
    precisions = 0
    f_measures = 0
    counter = 0

    for i, original_dataset in enumerate(original_datasets):
        if not original_dataset.is_summarize:
            article = json.loads(original_dataset.json_line)
            f_measure, precision, recall = article_rouge_evaluation(
                original_dataset, BASE_DIR)

            recalls += original_dataset.summaryevaluation.recall
            precisions += original_dataset.summaryevaluation.precision
            f_measures += original_dataset.summaryevaluation.f_measure

            print(
                f"article number {i}, recall = {recall}, precision = {precision}, f_measure = {f_measure}\n")
        else:
            print(f"article number {i}, recall = {original_dataset.summaryevaluation.recall}, precision = {original_dataset.summaryevaluation.precision}, f_measure = {original_dataset.summaryevaluation.f_measure}\n")
            
            recalls += original_dataset.summaryevaluation.recall
            precisions += original_dataset.summaryevaluation.precision
            f_measures += original_dataset.summaryevaluation.f_measure
        counter += 1

    mean_recall = recalls / counter
    mean_precision = precisions / counter
    mean_f_measure = f_measures / counter

    end = time()
    result = end-start
    result = str(datetime.timedelta(seconds=result))

    DatasetSummaryEvaluation.objects.create(
        type_dataset=testingdatasetsource, recall=mean_recall, precision=mean_precision, f_measure=mean_f_measure, execution_time=result)

    TestingDatasetSources.objects.filter(
        pk=testingdatasetsource.id).update(is_evaluate=True)

