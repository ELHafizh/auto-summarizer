from django.shortcuts import render
from django.http import HttpRequest, HttpResponseRedirect
from .models import DatasetSources, OriginalDataset, AggregateResource, TestingDatasetSources, TestingOriginalDataset, SelectedRule, LogProcess, SummaryEvaluation
from django.core.paginator import EmptyPage, PageNotAnInteger, Paginator
from .functionality.sentence_domain import read_json_line, iterate_filedecorator, saved_originaldataset, \
    echo_sentence_label_pairing, SentenceMunging, SentenceScoring, dataset_into_dataframe, ComprehendDataset, \
    dataframe_into_csv, dataframe_into_database, Bagging, RandomForest, save_rules, saved_test_originaldataset, \
    concat_tokens_tosentence, evaluate_oob, extract_ground_truth, article_rouge_evaluation, proceed_rouge_evaluation
import os
import json
import socket
from django.urls import reverse
from os import listdir, makedirs
import pandas as pd
from time import time
import datetime
from natsort import natsorted
from django.utils import timezone
from django.core.exceptions import ObjectDoesNotExist

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
dataset_dir = "/summarizer_project/static/"

global_context = {}

def index(request, dataset_id="none"):
    check_dataset = TestingDatasetSources.objects.all()
    if not check_dataset:
        return render(request, 'inference_engine/index.html', {
            "dataset_empty": True
        })
    if dataset_id != "none":
        dataset_res = TestingDatasetSources.objects.get(pk=dataset_id)
    else:
        dataset_id = TestingDatasetSources.objects.all()[0].id
        dataset_res = TestingDatasetSources.objects.get(pk=dataset_id)
    original_datasets = dataset_res.testingoriginaldataset_set.all()
    ids = []
    categories = []
    titles = []
    contents = []
    extractive_summaries = []
    sources = []
    source_urls = []
    for original_dataset in original_datasets:
        raw_jsonline = json.loads(original_dataset.json_line)
        ids.append(original_dataset.id)
        categories.append(original_dataset.category)
        titles.append(original_dataset.title)
        contents.append(concat_tokens_tosentence(raw_jsonline['paragraphs']))
        sources.append(original_dataset.source)
        source_urls.append(raw_jsonline['source_url'])
        extractive_summaries.append(original_dataset.reference_summary)
    compact_articles = zip(ids, categories, titles, contents, sources, source_urls, extractive_summaries)
    paginator = Paginator(list(compact_articles), 6)
    page = request.GET.get('page')
    paged_article = paginator.get_page(page)
    available_datasets = TestingDatasetSources.objects.all()
    context = {
        "dataset_empty": False,
        "compact_articles": paged_article,
        "available_datasets": available_datasets,
        "last_page": paginator.num_pages,
        "dataset_res": dataset_res,
        "is_evaluate": dataset_res.is_evaluate
    }
    return render(request, 'inference_engine/index.html', context)


def dataset_list(request):
    if request.method == 'POST':
        uploaded_file = request.FILES['rawdataset']
        print(uploaded_file.name)
        print(uploaded_file.size)
        rawdataset = DatasetSources(path=uploaded_file)
        rawdataset.save()
        filename = f"{uploaded_file.name}-{rawdataset.id}"
        file_path = BASE_DIR + dataset_dir + str(rawdataset.path)
        lines_injson = read_json_line(file_path)
        count_lines = iterate_filedecorator(lines_injson)
        DatasetSources.objects.filter(pk=rawdataset.id).update(title=filename)
        DatasetSources.objects.filter(pk=rawdataset.id).update(total_articles=count_lines)
        saved_originaldataset(rawdataset, file_path, count_lines)
    rules = AggregateResource.objects.all()
    context = {
        'dataset_sources': DatasetSources.objects.all(),
        'rules': rules
    }
    return render(request, 'inference_engine/dataset_list.html', context)


def action_step_datasets(request, dataset_id=''):
    if request.method == 'POST':
        dataset_id = request.POST['dataset_id']
        dataset_res = DatasetSources.objects.get(pk=dataset_id)
        if dataset_res.is_evaluate_oob:
            context = {
                'dataset_res': dataset_res,
                'accuracy_oob': round(dataset_res.aggregateresource.accuracy_oob, 2)
            }
        else:
            context = {
                'dataset_res': dataset_res
            }
    else:
        dataset_id = dataset_id
        dataset_res = DatasetSources.objects.get(pk=dataset_id)
        context = {
            'dataset_res': dataset_res
        }
    return render(request, 'inference_engine/action_step.html', context)


def pairing_sentence_label(request):
    if request.method == 'POST':
        start = time()

        dataset_id = request.POST['dataset_id']
        dataset_res = DatasetSources.objects.get(pk=dataset_id)
        original_datasets = dataset_res.originaldataset_set.all()
        for original_dataset in original_datasets:
            article = json.loads(original_dataset.json_line)
            results = echo_sentence_label_pairing(article
                                                  ['paragraphs'], article['gold_labels'])
            for result in results:
                related_originaldataset = OriginalDataset.objects.get(pk=original_dataset.id)
                related_originaldataset.trainingdataset_set.create(sentence_token=result[0], sentence=result[1],
                                                                   label=result[2])
        DatasetSources.objects.filter(pk=dataset_res.id).update(is_originaldatasetpair=True)
        global_context = {
            'dataset_res': dataset_res
        }

        ground_truth = extract_ground_truth(dataset_id)
        DatasetSources.objects.filter(pk=dataset_res.id).update(ground_truth=ground_truth)

        end = time()
        result = end-start
        print(f"inference_engine:pairing_sentence_label takes = {str(datetime.timedelta(seconds=result))}")

        return HttpResponseRedirect(reverse('inference_engine:dataset-process', args=[dataset_id]))


def display_training_dataset(request, dataset_id):
    if request.method == 'GET':
        start = time()

        dataset_res = DatasetSources.objects.get(pk=dataset_id)
        original_datasets = dataset_res.originaldataset_set.all()
        sentence_tokens = []
        sentences = []
        labels = []
        for original_dataset in original_datasets[::-1]:
            related_original_dataset = OriginalDataset.objects.get(pk=original_dataset.id)
            training_datasets = related_original_dataset.trainingdataset_set.all()
            for training_dataset in training_datasets:
                sentence_tokens.append(training_dataset.sentence_token)
                sentences.append(training_dataset.sentence)
                labels.append(training_dataset.label)
        compact_sentences = zip(sentences, sentence_tokens, labels)
        paginator = Paginator(list(compact_sentences), 20)
        page = request.GET.get('page')
        paged_sentences = paginator.get_page(page)
        context = {
            "compact_sentences": paged_sentences,
            "last_page": paginator.num_pages
        }

        end = time()
        result = end-start
        print(f"inference_engine:display_training_dataset takes = {str(datetime.timedelta(seconds=result))}")
        return render(request, 'inference_engine/display_trainingdata.html', context)


def sentence_munging(request):
    if request.method == 'POST':
        start = time()

        dataset_id = request.POST['dataset_id']
        dataset_res = DatasetSources.objects.get(pk=dataset_id)
        original_datasets = dataset_res.originaldataset_set.all()
        for original_dataset in original_datasets:
            article = json.loads(original_dataset.json_line)
            results = echo_sentence_label_pairing(article['paragraphs'], article['gold_labels'])
            for result in results:
                related_originaldataset = OriginalDataset.objects.get(pk=original_dataset.id)
                sentence_munging = SentenceMunging(result[0])
                related_originaldataset.preprocesstrainingdataset_set.create(sentence_token=sentence_munging.tokens,
                                                                             sentence=result[1], label=result[2])
        DatasetSources.objects.filter(pk=dataset_id).update(is_preprocessing=True)

        end = time()
        result = end-start
        print(f"inference_engine:sentence_munging takes = {str(datetime.timedelta(seconds=result))}")
        return HttpResponseRedirect(reverse('inference_engine:dataset-process', args=[dataset_id]))


def display_sentence_munging(request, dataset_id):
    if request.method == 'GET':
        start = time()

        dataset_res = DatasetSources.objects.get(pk=dataset_id)
        original_datasets = dataset_res.originaldataset_set.all()
        sentence_tokens = []
        sentences = []
        labels = []
        for original_dataset in original_datasets[::-1]:
            related_original_dataset = OriginalDataset.objects.get(pk=original_dataset.id)
            training_datasets = related_original_dataset.preprocesstrainingdataset_set.all()
            for training_dataset in training_datasets:
                sentence_tokens.append(training_dataset.sentence_token)
                sentences.append(training_dataset.sentence)
                labels.append(training_dataset.label)
        compact_sentences = zip(sentences, sentence_tokens, labels)
        paginator = Paginator(list(compact_sentences), 20)
        page = request.GET.get('page')
        paged_sentences = paginator.get_page(page)
        context = {
            "compact_sentences": paged_sentences,
            "last_page": paginator.num_pages
        }

        end = time()
        result = end-start
        print(f"inference_engine:display_sentence_munging takes = {str(datetime.timedelta(seconds=result))}")
        return render(request, 'inference_engine/display_sentencemunging.html', context)


def sentence_scoring(request):
    if request.method == 'POST':
        start = time()

        dataset_id = request.POST['dataset_id']
        dataset_res = DatasetSources.objects.get(pk=dataset_id)
        original_datasets = dataset_res.originaldataset_set.all()
        for original_dataset in original_datasets:
            related_original_dataset = OriginalDataset.objects.get(pk=original_dataset.id)
            preprocessing_datasets = related_original_dataset.preprocesstrainingdataset_set.all()
            articles = []
            sentences = []
            labels = []
            for preprocessing_dataset in preprocessing_datasets:
                articles.append(preprocessing_dataset.sentence_token)
                sentences.append(preprocessing_dataset.sentence)
                labels.append(preprocessing_dataset.label)
            sentence_scoring = SentenceScoring(related_original_dataset.title, articles)
            val_zip = zip(sentences, sentence_scoring.score['f1_wf'], sentence_scoring.score['f2_ts'],
                          sentence_scoring.score['f3_sp'], sentence_scoring.score['f4_sl'],
                          sentence_scoring.score['f5_centrality'], sentence_scoring.score['f6_tfisf'],
                          sentence_scoring.score['f7_cosim'], sentence_scoring.score['f8_bigram'],
                          sentence_scoring.score['f9_trigram'], labels)
            for sentence, f1, f2, f3, f4, f5, f6, f7, f8, f9, label in val_zip:
                related_original_dataset.sentencescoring_set.create(sentence=sentence, f1_word_frequency=f1,
                                                                    f2_title_similarity=f2, f3_sentence_position=f3,
                                                                    f4_sentence_length=f4, f5_centrality=f5,
                                                                    f6_tf_isf=f6, f7_cosim=f7, f8_bigram=f8,
                                                                    f9_trigram=f9, label=label)
        DatasetSources.objects.filter(pk=dataset_id).update(is_sentencescoring=True)

        end = time()
        result = end-start
        print(f"inference_engine:sentence_scoring takes = {str(datetime.timedelta(seconds=result))}")
        return HttpResponseRedirect(reverse('inference_engine:dataset-process', args=[dataset_id]))


def display_sentence_scoring(request, dataset_id):
    if request.method == 'GET':
        start = time()

        dataset_res = DatasetSources.objects.get(pk=dataset_id)
        original_datasets = dataset_res.originaldataset_set.all()
        sentences, f1, f2, f3, f4, f5, f6, f7, f8, f9, labels = [], [], [], [], [], [], [], [], [], [], [],
        for original_dataset in original_datasets:
            related_original_dataset = OriginalDataset.objects.get(pk=original_dataset.id)
            sentence_scorings = related_original_dataset.sentencescoring_set.all()
            for sentence_scoring in sentence_scorings[::-1]:
                sentences.append(sentence_scoring.sentence)
                f1.append(sentence_scoring.f1_word_frequency)
                f2.append(sentence_scoring.f2_title_similarity)
                f3.append(sentence_scoring.f3_sentence_position)
                f4.append(sentence_scoring.f4_sentence_length)
                f5.append(sentence_scoring.f5_centrality)
                f6.append(sentence_scoring.f6_tf_isf)
                f7.append(sentence_scoring.f7_cosim)
                f8.append(sentence_scoring.f8_bigram)
                f9.append(sentence_scoring.f9_trigram)
                labels.append(sentence_scoring.label)
        compact_scores = zip(sentences, f1, f2, f3, f4, f5, f6, f7, f8, f9, labels)
        paginator = Paginator(list(compact_scores), 50)
        page = request.GET.get('page')
        paged_scores = paginator.get_page(page)
        context = {
            "compact_scores": paged_scores,
            "last_page": paginator.num_pages
        }

        end = time()
        result = end-start
        print(f"inference_engine:display_sentence_scoring takes = {str(datetime.timedelta(seconds=result))}")
        return render(request, 'inference_engine/display_sentencescoring.html', context)


def feature_scaling(request):
    if request.method == 'POST':
        start = time()

        dataset_id = request.POST['dataset_id']
        dataset_res = DatasetSources.objects.get(pk=dataset_id)
        original_datasets = dataset_res.originaldataset_set.all()
        df, sentences = dataset_into_dataframe(original_datasets)
        comprehend_dataset = ComprehendDataset(df)
        comprehend_dataset.begin_normalization()
        dataframe_into_csv(comprehend_dataset.dataset, BASE_DIR, dataset_res.title, dataset_id)
        comprehend_dataset.final_normalization()
        dataframe_into_csv(comprehend_dataset.dataset, BASE_DIR, dataset_res.title, dataset_id, 1)
        dataframe_into_database(comprehend_dataset.dataset, sentences, dataset_id, original_datasets)

        end = time()
        result = end-start
        print(f"inference_engine:feature_scaling takes = {str(datetime.timedelta(seconds=result))}")
        return HttpResponseRedirect(reverse('inference_engine:dataset-process', args=[dataset_id]))


def display_feature_scaling(request, dataset_id):
    if request.method == 'GET':
        start = time()

        dataset_res = DatasetSources.objects.get(pk=dataset_id)
        original_datasets = dataset_res.originaldataset_set.all()
        sentences, f1, f2, f3, f4, f5, f6, f7, f8, f9, labels = [], [], [], [], [], [], [], [], [], [], [],
        for original_dataset in original_datasets:
            related_original_dataset = OriginalDataset.objects.get(pk=original_dataset.id)
            sentence_scorings = related_original_dataset.preprocesssentencescoring_set.all()
            for sentence_scoring in sentence_scorings[::-1]:
                sentences.append(sentence_scoring.sentence)
                f1.append(round(sentence_scoring.f1_word_frequency, 2))
                f2.append(round(sentence_scoring.f2_title_similarity, 2))
                f3.append(round(sentence_scoring.f3_sentence_position, 2))
                f4.append(round(sentence_scoring.f4_sentence_length, 2))
                f5.append(round(sentence_scoring.f5_centrality, 2))
                f6.append(round(sentence_scoring.f6_tf_isf, 2))
                f7.append(round(sentence_scoring.f7_cosim, 2))
                f8.append(round(sentence_scoring.f8_bigram, 2))
                f9.append(round(sentence_scoring.f9_trigram, 2))
                labels.append(sentence_scoring.labels)
        compact_scores = zip(sentences, f1, f2, f3, f4, f5, f6, f7, f8, f9, labels)
        paginator = Paginator(list(compact_scores), 50)
        page = request.GET.get('page')
        paged_scores = paginator.get_page(page)
        context = {
            "compact_scores": paged_scores,
            "last_page": paginator.num_pages
        }

        end = time()
        result = end-start
        print(f"inference_engine:display_feature_scaling takes = {str(datetime.timedelta(seconds=result))}")
        return render(request, 'inference_engine/display_featurescaling.html', context)


def proceed_bagging(request):
    if request.method == 'POST':
        start = time()

        dataset_id = request.POST['dataset_id']
        dataset_res = DatasetSources.objects.get(pk=dataset_id)
        dataset_dir = f"{BASE_DIR}{dataset_res.aggregateresource.scaled_sentencescoring_path}"
        df = pd.read_csv(dataset_dir)
        bagging = Bagging(df)
        index_file = 0
        for bag, oob in zip(bagging.listof_bagging, bagging.listof_oob):
            start_save_bag = time()

            dataframe_into_csv(bag, BASE_DIR, dataset_res.title, dataset_id, 2, index_file)
            dataframe_into_csv(oob, BASE_DIR, dataset_res.title, dataset_id, 3, index_file)

            end_save_bag = time()
            result_save_bag = end_save_bag - start_save_bag
            result_save_bag = str(datetime.timedelta(seconds=result_save_bag))

            print(f"inference_engine:bags_to_csv takes = {result_save_bag}")

            index_file += 1
        DatasetSources.objects.filter(pk=dataset_id).update(is_bagging=True)

        end = time()
        result = end-start
        result = str(datetime.timedelta(seconds=result))
        print(f"inference_engine:proceed_100_bagging takes = {result}")

        log_process = LogProcess(dataset_name=dataset_res.title, process_name='proceed_bagging', execution_time=result)
        log_process.save()

        return HttpResponseRedirect(reverse('inference_engine:dataset-process', args=[dataset_id]))


def random_forest(request):
    if request.method == 'POST':
        start = time()

        dataset_id = request.POST['dataset_id']
        dataset_res = DatasetSources.objects.get(pk=dataset_id)
        bag_path = dataset_res.aggregateresource.bag_path
        bag_filenames = listdir(f"{BASE_DIR}{bag_path}")
        list_of_bags = []
        for bag_filename in bag_filenames:
            file_path_loc = f"{BASE_DIR}{bag_path}/{bag_filename}"
            df = pd.read_csv(file_path_loc)
            list_of_bags.append(df)
        randomforest = RandomForest(list_of_bags, dataset_name=dataset_res.title)
        rules_dir = f"/summarizer_project/static/rules/{dataset_res.title}"
        save_rules(randomforest.rules, BASE_DIR, rules_dir, dataset_id)

        end = time()
        result = end-start
        result = str(datetime.timedelta(seconds=result))
        print(f"inference_engine:random_forest takes = {result}")

        log_process = LogProcess(dataset_name=dataset_res.title, process_name='random_forest', execution_time=result)
        log_process.save()

        return HttpResponseRedirect(reverse('inference_engine:dataset-process', args=[dataset_id]))


def get_accuracy_from_oob(request):
    if request.method == 'POST':
        start = time()

        dataset_id = request.POST['dataset_id']
        dataset_res = DatasetSources.objects.get(pk=dataset_id)

        oob_path = dataset_res.aggregateresource.oob_path
        selected_rule = SelectedRule.objects.get(
            pk=SelectedRule.objects.all()[0].id)
        rules_path = selected_rule.current_rule
        rules_path = f"{BASE_DIR}{rules_path}"

        loc_file = f"{BASE_DIR}{oob_path}"
        oob_filenames = listdir(f"{loc_file}")
        oob_filenames = natsorted(oob_filenames)

        for i, oob_file in enumerate(oob_filenames):
            oob_filenames[i] = f"{loc_file}/{oob_file}"

        for i, oob_filename in enumerate(oob_filenames):
            start_single_oob = time()

            accuracy_score = evaluate_oob(oob_filename, rules_path)

            end_single_oob = time()
            result_single_oob = end_single_oob - start_single_oob
            result_single_oob = str(
                datetime.timedelta(seconds=result_single_oob))

            dataset_res.logoobaccuracy_set.create(
                dataset_name=f"{dataset_res.title}_oob_{i}", accuracy=accuracy_score, execution_date=timezone.now(), execution_time=result_single_oob)

            print(
                f"looking for accuracy from oob-{i} takes = {result_single_oob}")

        oob_accuracy_scores = dataset_res.logoobaccuracy_set.all()
        scores = []
        for oob_accuracy_score in oob_accuracy_scores:
            scores.append(oob_accuracy_score.accuracy)

        total_score = 0
        for score in scores:
            total_score += score

        total_score = total_score / len(scores)

        end = time()
        result = end-start
        result = str(datetime.timedelta(seconds=result))

        AggregateResource.objects.filter(pk=dataset_id).update(
            accuracy_oob=total_score, accuracy_execution_time=result)

        DatasetSources.objects.filter(pk=dataset_id).update(is_evaluate_oob=True)

        print(f"inference_engine:evaluate-oob takes = {result}")

        log_process = LogProcess(dataset_name=dataset_res.title,
                                 process_name='get_accuracy_from_oob', execution_time=result)
        log_process.save()

        return HttpResponseRedirect(reverse('inference_engine:dataset-process', args=[dataset_id]))


"""
    Testing Section
"""


def upload_test_dataset(request):
    if request.method == 'POST':
        uploaded_file = request.FILES['testdataset']
        print(uploaded_file.name)
        print(uploaded_file.size)
        testdataset = TestingDatasetSources(path=uploaded_file)
        testdataset.save()
        filename = f"{uploaded_file.name}-{testdataset.id}"
        file_path = BASE_DIR + dataset_dir + str(testdataset.path)
        lines_injson = read_json_line(file_path)
        count_lines = iterate_filedecorator(lines_injson)
        TestingDatasetSources.objects.filter(pk=testdataset.id).update(title=filename)
        TestingDatasetSources.objects.filter(pk=testdataset.id).update(total_articles=count_lines)
        saved_test_originaldataset(testdataset, file_path, count_lines)
        return HttpResponseRedirect(reverse('inference_engine:index', args=[testdataset.id]))


def summarize_article(request, article_id):
    if request.method == 'GET':
        original_dataset = TestingOriginalDataset.objects.get(pk=article_id)
        raw_jsonline = json.loads(original_dataset.json_line)
        try:
            summary_evaluation = original_dataset.summaryevaluation
        except (ObjectDoesNotExist):
            summary_evaluation = False
        if summary_evaluation:
            context = {
                'dataset_id': original_dataset.id,
                'content': concat_tokens_tosentence(raw_jsonline['paragraphs']),
                'source_url': raw_jsonline['source_url'],
                'reference_summary': original_dataset.reference_summary,
                'is_summarize': original_dataset.is_summarize,
                'system_summary': original_dataset.system_summary,
                'summary_evaluation' : summary_evaluation,
                'recall' : round(summary_evaluation.recall, 2),
                'precision' : round(summary_evaluation.precision, 2),
                'f_measure' : round(summary_evaluation.f_measure, 2)
            }
        else:
            context = {
                'dataset_id': original_dataset.id,
                'content': concat_tokens_tosentence(raw_jsonline['paragraphs']),
                'source_url': raw_jsonline['source_url'],
                'reference_summary': original_dataset.reference_summary,
                'is_summarize': original_dataset.is_summarize,
                'system_summary': original_dataset.system_summary,
                'summary_evaluation' : summary_evaluation,
            }
        return render(request, 'inference_engine/summarize_article.html', context)


def update_rule(request, rule_id):
    if request.method == 'GET':
        aggregateresource = AggregateResource.objects.get(pk=rule_id)
        dataset_res = DatasetSources.objects.get(pk=rule_id)
        selectedrules = SelectedRule.objects.all()
        selectedrule_id = selectedrules[0].id
        selectedrule = SelectedRule.objects.get(pk=selectedrule_id)
        selectedrule.current_rule = aggregateresource.rules_path
        selectedrule.fit_test_path = aggregateresource.fit_test_path
        selectedrule.ground_truth = dataset_res.ground_truth
        selectedrule.save()
        return HttpResponseRedirect(reverse('inference_engine:dataset-list'))


def test_generate_summary(request, dataset_id):
    if request.method == 'GET':
        original_dataset = TestingOriginalDataset.objects.get(pk=dataset_id)
        article_rouge_evaluation(original_dataset, BASE_DIR)
        return HttpResponseRedirect(reverse('inference_engine:summarize-article', args=[dataset_id]))


def test_generate_summaries(request, dataset_id):
    if request.method == 'GET':
        start = time()

        dataset_res = TestingDatasetSources.objects.get(pk=dataset_id)
        proceed_rouge_evaluation(dataset_res, BASE_DIR)

        end = time()
        result = end-start
        result = str(datetime.timedelta(seconds=result))

        LogProcess.objects.create(dataset_name=dataset_res.title, process_name="test_generate_summaries",
                                  execution_date=timezone.now(), execution_time=result)

        return HttpResponseRedirect(reverse('inference_engine:index', args=[dataset_id]))


def proceed_statistics(request, dataset_id):
    if request.method == 'GET':
        
        dataset_res = TestingDatasetSources.objects.get(pk=dataset_id)
        original_datasets = dataset_res.testingoriginaldataset_set.all()
        
        ids, titles, recalls, precisions, f_measures = [], [], [], [], []
        for original_dataset in original_datasets:
            ids.append(original_dataset.id)
            titles.append(original_dataset.title)
            recalls.append(round(original_dataset.summaryevaluation.recall, 2))
            precisions.append(round(original_dataset.summaryevaluation.precision, 2))
            f_measures.append(round(original_dataset.summaryevaluation.f_measure, 2))
        
        compact_scores = zip(ids, titles, recalls, precisions, f_measures)
        
        paginator = Paginator(list(compact_scores), 10)
        page = request.GET.get('page')
        paged_scores = paginator.get_page(page)
        context = {
            "compact_scores": paged_scores,
            "last_page": paginator.num_pages,
            "recall": round(dataset_res.datasetsummaryevaluation.recall, 2),
            "precision": round(dataset_res.datasetsummaryevaluation.precision, 2),
            "f_measure": round(dataset_res.datasetsummaryevaluation.f_measure, 2)
        }

        return render(request, 'inference_engine/test_dataset_statistics.html', context)


"""
Time Tracker
# start = time()

# end = time()
# result = end-start
# print(f"inference_engine: takes = {str(datetime.timedelta(seconds=result))}")
"""
