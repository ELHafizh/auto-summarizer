# Generated by Django 2.2.7 on 2020-03-02 22:15

import datetime
from django.db import migrations, models
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('inference_engine', '0003_auto_20200303_0514'),
    ]

    operations = [
        migrations.AlterField(
            model_name='logprocess',
            name='execution_date',
            field=models.DateTimeField(blank=True, default=datetime.datetime(2020, 3, 2, 22, 15, 3, 115301, tzinfo=utc)),
        ),
    ]
