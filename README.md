# An auto summarizer app for undergraduate thesis

## Requirements

- Python>=3.6
  - Tested on Python version 3.7.x (Linux & Windows)
- PostgreSQL

## Installation

- Install virtualenv globally

  ```
  >> python -m pip install virtualenv
  ```

- Create virtual environment

  ```
  >> python -m venv ./venv-auto-summarizer
  ```

- Activate environment

  - Linux

    ```
    >> source ./venv-auto-summarizer/bin/activate
    ```

  - Windows

    ```
    >> .\venv-auto-summarizer\Scripts\activate.bat
    ```

- Install dependencies

  ```
  >> pip install -r requirements.txt
  ```

- Setup Database Configuration

  **[ summarizer_project/summarizer_project/settings.py ]**

  ```python
  DATABASES = {
      'default': {
          'ENGINE': 'django.db.backends.postgresql',
          'NAME': 'auto_summarizer_db',
          'USER': 'postgres',
          'PASSWORD': 'password',
          'HOST': 'localhost'
      }
  }
  ```

-------------------

- Change directory into `./summarizer_project`

  ```
  >> cd summarizer_project
  ```

- Perform migration

  ```
  >> python manage.py migrate inference_engine
  ```

- Running server

  ```
  >> python manage runserver
  ```

  